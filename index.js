console.log("Hello World")



for (let i = (prompt("Enter Number:")); i >= 0; i--) { console.log(i); 
/*
}

for (let count = (prompt("Your Number:")); count <=100; count-=5) { console.log(count); 

	if (count % 10 === 0) {
		continue;
	}
	console.log("The number is divisible by 10. Skipping the number." + count);
/*
	if (i % 5 === 0) {
		continue;
	}
	console.log(i);
*/

	if (i <= 50) {
		console.log("The current value is at  " + i + " .Terminating the loop");
		break;

	} else if (i % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number." + i);
		continue;
	} else if (i % 5 === 0) {
		console.log(i);
	}
}


let string = "supercalifragilisticexpialidocious";
	console.log(string);
	let filteredString = " ";

//Create a loop that will iterate throught the whole string
for (let v=0; v < string.length; v++){
	//check what is the starting value of the loop
	//console.log(string[1]);

	//if the current letter is being evaluated as vowel
	/*
		iterate 1:
			string[0].toLowerCase() == 'a'
			check: is "s" == "a"
			else: filteredString += "s"

		iterate 2:
			v is now 1
			string[1.toLowerCase() == "a" || "e" || "i" || "o" || "u"]
			check: "u" == "u"
			continue = skip to the next iteration
			filteredString = "s"

			iteration 3:
			v is now 2
			string[1.toLowerCase() == "a" || "e" || "i" || "o" || "u"]
			check: "p" !== "a" || "e" || "i" || "o" || "u"
			else: filteredString += "p"
			filteredString += "sp"
	*/
		if (
			string[v].toLowerCase() == "a" ||
			string[v].toLowerCase() == "e" ||
			string[v].toLowerCase() == "i" ||
			string[v].toLowerCase() == "o" ||
			string[v].toLowerCase() == "u"	
			) {
			// continue the loop to the next letter/character in the sequence
			continue;
			//if the current letter being evaluated is not a vowel
		} else {
			// add the letter to a different variable
			filteredString += string[v];
		}
}
// after the loop is complete. print the filtered string without the vowels
console.log(filteredString);

